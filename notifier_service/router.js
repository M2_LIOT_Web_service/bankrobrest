const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require('swagger-ui-express');
const service = require('./service/notifierService');
const func = require('./fn/function');

/**
 * Définition des options Swagger
 */
//#region swagger
const swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
          title: "Notifier Rest API",
          version: "1.0.0",
          description: "Simple Notifier management API.",
          contact: {
            name: "BankRob",
          },
          servers: ["http://localhost:" + func.normalizePort(process.env.NOTIFIER_PORT || '8022')],
        },
      },
      apis: ["router.js"], // files containing annotations as above
};
const swaggerDocs = swaggerJsdoc(swaggerOptions);
//#endregion

router.use("/api/v1/notifier/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

/**
 * @openapi
 * /api/v1/notifier:
 *   post:
 *     description: To send notification on user's device.
 *     tags:
 *       - Notification
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: title and message are required.
 *             type: object
 *             required:
 *               - title
 *               - message
 *             properties:
 *               title:
 *                 type: string
 *               message:
 *                 type: string
 *     responses:
 *       400:
 *          description: Error, title or message is empty.
 *       200:
 *         description: Return  "notification has been sent".
 *       500:
 *         description: Error message.
 */
router.post('/api/v1/notifier', service.sendNotification);

/**
 * @openapi
 * /api/v1/notifier/email:
 *   post:
 *     description: To send email.
 *     produces:
 *       - application/json
 *     tags:
 *       - Email
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: from and to are required.
 *             type: object
 *             required:
 *               - to
 *             properties:
 *               to:
 *                 type: string
*               subject:
 *                 type: string
 *               text:
 *                 type: string
 *     responses:
 *       400:
 *          description: Error, from or to is empty.
 *       200:
 *         description: Return  "email has been sent".
 *       500:
 *         description: Error message.
 */
router.post('/api/v1/notifier/email', service.sendEmail);

router.use('/', (req, res, next) => {
    if (req.url == '/') {
        res.redirect('/api/v1/notifier/api-docs');
    } else {
        next();
    }
});

router.use((req, res) => {
    res.status(404).send('Page non trouvée');
});

module.exports = router;