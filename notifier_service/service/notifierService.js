var nodemailer = require("nodemailer");
const notifier = require('node-notifier');
const path = require('path');

// Description du service : Permet d’envoyer un message sous diverses formes (mail, message, image etc.).

var transporter = nodemailer.createTransport({
  service: "gmail",
  host: "smtp.gmail.com",
  // port: 587,
  secure: false,
  requireTLS: true,
  auth: {
    user: "bankrobapi@gmail.com",
    pass: "bankrobynov",
  },
});

/**
 * @summary Envoi un message
 * @param {json} req Titre et message
 * @param {json} res Un message de succès ou d'erreur en json
 * @returns {statut} Le statut avec un message json
 */
exports.sendNotification = async (req,res) => {
  const title = req.body.title;
  const message = req.body.message;
  if(!title || !message){
    return  res.status(400).json('{ message: "Erreur : title & message must be filled" }');
  }
  var notifierOption = {
    title : title,
    message : message,
    icon: path.join(__dirname, 'icon_bank_rob.jpg'),
    appID: "BankRob"
  }
  console.log("Send notification req.body : ", req.body);
  notifier.notify(notifierOption, function(error, response){
    if(error){
      console.log("notify error : ", error);
      return res.status(500).json('{ message: error }');
    } else {
      console.log("Notification sent: " + response);
      return res.status(200).json('{ message: "Notification has been sent" }');
    }
  });
}

/**
 * @summary Envoi un email
 * @param {json} req l'adresse de l'expéditeur et celle du destinataire, le sujet et le contenu du mail
 * @param {json} res Un message de succès ou d'erreur en json
 * @returns {statut} Le statut avec un message json
 */
exports.sendEmail = async (req,res) => {
  console.log("req.body : ", req.body);
  const to = req.body.to;
  const subject = req.body.subject;
  const text = req.body.text;
  if (!to) {
    return  res.status(400).json('{ message: "to must be filled" }');
  }
  var mailOptions = {
    to: to,
    subject: subject,
    text: text
  };
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log("sendMail : ", error);
      return res.status(500).json('{ message: error }');
    } else {
      console.log("Email sent: " + info.response);
      return res.status(200).json('{ message: "Email has been sent" }');
    }
  });
}