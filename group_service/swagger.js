const swaggerJsdoc = require("swagger-jsdoc");

/**
 * Définition des options Swagger
 */
//#region swagger
const swaggerOptions = {
    definition: {
      openapi: "3.0.0",
      info: {
        title: "Group Rest API",
        version: "1.0.0",
        description: "Simple Group management API.",
        contact: {
          name: "BankRob",
        },
        servers: [`http://localhost:${process.env.PORT}`],
      },
    },
    apis: ["router.js"], // files containing annotations as above
  };
  

module.exports = swaggerJsdoc(swaggerOptions);
  //#endregion