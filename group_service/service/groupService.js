const Group = require('../models/group');
const Association = require('../models/association');
const mongoose = require('mongoose');

// Description du service : Permet de gérer un groupe (ex : braquage), y compris l’ajout ou la suppression des membres.

/**
 * @summary Permet de récupérer les groupes
 * @param {json} res Envoi les groupes ou bien envoi un message d'erreur au format json
 * @returns {statut} Les groupes et le statut de la requête
 */
exports.getGroups = async (req,res) => {
    Group.find()
    .exec()
    .then(group => {
        if(group && group.length > 0){
            return res.status(200).json(group);
        }
        else {
            return res.status(204).json(errorJson('Not Groups in database'));
        }
    })
    .catch(err => res.status(500).json(errorJson(err)));
}

/**
 * @summary Récupère un groupe en fonction de son ID
 * @param {string} req Id du groupe
 * @param {json} res Envoi le groupe ou bien un message d'erreur au format json
 * @returns {statut} Le groupe et le message associé
 */
exports.getById = async (req, res) => {
    const id = req.params.id;
    try{
        // get associations
        let asso = await Association.find({ id_group : id});
        console.log("association : ", asso);
        // get group
        let group = await Group.findById({_id : id}).lean();
        console.log('group : ', group);
        if(group){
            group.association = asso;
            res.status(200).json(group);
        } else {
            res.status(404).json(errorJson("group with id : " + id + " does not exist."));
        }
    } 
    catch(err){
        res.status(500).json(errorJson(err));
    }
}

/**
 * @summary Permet d'ajouter un groupe
 * @param {string} req Id et nom du groupe
 * @param {json} res Message de succès avec le groupe ajouté ou message d'erreur
 * @returns {statut} Le groupe et le message associé
 */
exports.addGroup = async (req, res) => {
    const _id = mongoose.Types.ObjectId();
    const group = new Group({ _id:_id , ...req.body});
    group.save((err) => {
        if(err){
            return res.status(500).json(errorJson(err));
        }
        return res.status(201).json(group);
    });
}

/**
 * @summary Mets à jour un groupe
 * @param {string} req id du groupe à mettre à jour et son contenu
 * @param {json} res Statut et message json
 * @returns {statut} Renvoi le statut et le message json
 */
exports.update = (req, res) => {
    Group.findOneAndUpdate({_id: req.body._id}, req.body)
    .then(group => Group.findOne({_id:req.body._id})
    .then(newGroup => res.status(200).json(newGroup))
    .catch(err2 => res.status(500).json(errorJson(err2))))
    .catch(err => res.status(500).json(errorJson(err)));
}

/**
 * @summary Suppression d'un groupe
 * @param {string} req id du groupe à supprimer
 * @param {json} res Statut et message json
 * @returns {statut} Renvoi le statut message json
 */
exports.delete = async (req, res, next) => {
    const idGroup = req.params.id;
    try {
        // On supprime le groupe
        const result = await Group.findByIdAndDelete(idGroup);
        if(result && result._id){
            // La suppression du groupe a réussi on supprime les associations avec ce groupe
            const resAsso = await Association.deleteMany({id_group: idGroup});
            let nbAssoDelete = "";
            if(resAsso && resAsso.deletedCount){
                nbAssoDelete = resAsso.deletedCount;
            }
            return res.status(200).json(`Group ${result.name} has been deleted and ${nbAssoDelete} associations with this group`);
        }
        return res.status(404).json(errorJson(`Request failed ! Group with id: ${idGroup} not found.`));
    } catch (err) {
        return res.status(500).json(errorJson(errorJson(err)));
    }
}

/**
 * Formalize JSON error message
 * @param {*} err
 */
 errorJson = (err) => {
    return {
        message: 'Group API Request Error',
        error: err
    };
}