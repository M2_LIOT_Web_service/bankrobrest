const Association = require('../models/association');
const mongoose = require('mongoose');

/*-------------*/
/* ASSOCIATION */
/*-------------*/

/**
 * @summary Retourne les associations
 * @param {*} req
 * @param {*} res 
 */
 exports.getAssociations = async (req,res) => {
    try{
        const associations = await Association.find();
        if(associations && associations.length > 0){
            return res.status(200).json(associations);
        }
        else {
            return res.status(204).json(errorJson('Not Associations in database'));
        }
    }
    catch(err){
        return res.status(500).json(errorJson(err));
    }
}

/**
 * @summary Permet d'ajouter un membre à un groupe
 * @param {string} req id du groupe et id du membre
 * @param {json} res Statut et message json
 * @returns {statut} Renvoi le statut message json
 */
exports.associate = async (req, res) => {
    const _id = mongoose.Types.ObjectId();
    const idGroup = req.body.id_group;
    const idGroupError = !req.body.id_group ? 'L\'id du groupe est obligatoire' : '';
    const idMemberError = !req.body.id_member ? 'L\'id du membre est obligatoire' : '';
    if(idGroupError != '' || idMemberError != ''){
        let separate = '';
        if(idGroupError != '') {
            separate = ', ';
        }
        return res.status(400).json(errorJson(idGroupError + (idMemberError != '' ? separate + idMemberError : '')));
    }
    try{
        let asso = await Association.find(req.body);
        if(asso && asso.length > 0){
            return res.status(403).json(errorJson("This user is already in group with id : " + idGroup));
        } else {
            const newAssociation = new Association({ _id:_id , ...req.body});
            newAssociation.save((err)=>{
                if(err){
                    return res.status(500).json(errorJson(err));
                } else {
                    return res.status(201).json({ message : 'Le membre est associé au groupe ' + idGroup });
                }
            });
        }
    }catch(err){
        return res.status(500).json(errorJson(err));
    }
}

/**
 * @summary Permet de dissocier un membre du groupe
 * @param {string} req id du membre
 * @param {json} res Statut et message json
 * @returns {statut} Renvoi le statut message json
 */
exports.dissociate = async (req, res) => {
    Association.findOneAndDelete({_id : req.params.id})
    .exec()
    .then(result => {
        if(result && result._id){
            return res.status(200).json(`Association ${result._id} has been deleted`);
        }
        return res.status(404).json(errorJson('Request failed ! Association not found.'));
    })
    .catch(err => res.status(500).json(errorJson(err)));
}

/**
 * Formalize JSON error message
 * @param {*} err
 */
errorJson = (err) => {
    return {
        message: 'Group API Request Error',
        error: err
    };
}