const mongoose = require('mongoose');

const clientOptions = {
    socketTimeoutMS   : 30000,
    keepAlive         : true,
    useNewUrlParser   : true,
    useUnifiedTopology: true,
};

exports.InitDbConnection = async () => {
    try{
        await mongoose.connect(process.env.URL_GROUP_MONGO || 'mongodb://localhost:27018/', clientOptions);
        console.log('Connexion à mongo réussie');
    } catch (error) {
        console.log(`La connexion à mongo a échoué ${error}`);
        throw error;
    }
}