const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const swaggerUi = require("swagger-ui-express");
const swaggerDocs = require("./swagger");
const serviceGroup = require('./service/groupService');
const serviceAssociation = require('./service/associationService');

router.use("/api/v1/groups/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

/**
 * @openapi
 * /api/v1/groups:
 *   get:
 *     description: Returns all groups in database.
 *     tags:
 *       - Group
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Returns (JSON) all groups in database.
 *       204:
 *         description: Not Group in database.
 *       500:
 *         description: Internal Server Error
 */
router.get('/api/v1/groups', serviceGroup.getGroups);

router.use('/', (req, res, next) => {
    if (req.url == '/') {
        res.redirect('/api/v1/groups/api-docs');
    } else {
        next();
    }
});

/**
 * @openapi
 * /api/v1/groups/{id}:
 *   get:
 *     description: Get group by his id.
 *     tags:
 *       - Group
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *     responses:
 *       200:
 *         description: Return (JSON) group by his id.
 *       404:
 *         description: Group Not Found.
 *       500:
 *         description: Internal Server Error
 */
router.get('/api/v1/groups/:id', (req, res, next)=>{
  if(req.url == '/api/v1/groups/associations') {
    next();
  } else {
    serviceGroup.getById(req, res);
  }
});

 /**
 * @openapi
 * /api/v1/groups/{id}:
 *   delete:
 *     description: Delete Group by his id.
 *     tags:
 *       - Group
 *     parameters:
 *       - in: path
 *         name: id
 *     responses:
 *       201:
 *         description: Delete an user by his id.
 *       404:
 *         description: Group Not Found.
 *       501:
 *         description: Internal Server Error
 */
router.delete('/api/v1/groups/:id', serviceGroup.delete);

/**
 * @openapi
 * /api/v1/groups:
 *   post:
 *     description: Add Group in database.
 *     tags:
 *       - Group
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: name and owner are required.
 *             type: object
 *             required:
 *               - name
 *               - owner
 *             properties:
 *               name:
 *                 type: string
 *               owner:
 *                 type: string
 *     responses:
 *       201:
 *         description: Return (JSON) new Group.
 *       500:
 *         description: Internal Server Error
 */
router.post('/api/v1/groups', serviceGroup.addGroup);

/**
 * @openapi
 * /api/v1/groups:
 *   put:
 *     description: Modify a Group in database.
 *     tags:
 *       - Group
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: Modify a Group in database.
 *             type: object
 *             required:
 *               - _id
 *             properties:
 *               _id: 
 *                 type: string
 *               name:
 *                 type: string
 *               owner:
 *                 type: string
 *     responses:
 *       200:
 *         description: Return (JSON) modify Group.
 *       500:
 *         description: Internal Server Error
 */
router.put('/api/v1/groups', serviceGroup.update);

/**
 * @openapi
 * /api/v1/groups/associations:
 *   get:
 *     description: Returns all associations in database.
 *     tags:
 *       - Association
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Returns (JSON) all associations in database.
 *       204:
 *         description: Not Association in database.
 *       500:
 *         description: Internal Server Error
 */
router.get('/api/v1/groups/associations', serviceAssociation.getAssociations);

/**
 * @openapi
 * /api/v1/groups/associations:
 *   post:
 *     description: Create an Association.
 *     tags:
 *       - Association
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: id_group and id_member are required.
 *             type: object
 *             required:
 *               - id_group
 *               - id_member
 *             properties:
 *               id_group:
 *                 type: string
 *               id_member:
 *                 type: string
 *     responses:
 *       201:
 *         description: Return (JSON) new Association.
 *       400:
 *         description: Bad Request.
 *       403:
 *         description: Forbidden (generaly association between group and user already exists).
 *       500:
 *         description: Internal Server Error
 */
router.post('/api/v1/groups/associations', serviceAssociation.associate);

 /**
 * @openapi
 * /api/v1/groups/associations/{id}:
 *   delete:
 *     description: Delete association by his id.
 *     tags:
 *       - Association
 *     parameters:
 *       - in: path
 *         name: id
 *     responses:
 *       200:
 *         description: Delete an user by his id.
 *       404:
 *         description: Association Not Found.
 *       500:
 *         description: Internal Server Error
 */
router.delete('/api/v1/groups/associations/:id', serviceAssociation.dissociate);

router.use((req, res) => {
    res.status(404).send('Page non trouvée');
});

module.exports = router;