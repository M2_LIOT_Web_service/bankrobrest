const mongoose = require('mongoose');
//const bcrypt   = require('bcryptjs');

const Association = new mongoose.Schema({
    _id: { type:String },
    id_group: {
        type: String,
        trim: true,
        required: [true, 'L\'identifiant groupe est obligatoire']
    },
    id_member: {
        type: String,
        trim: true,
        required: [true, 'L\'identifiant membre est obligatoire']
    }
});

module.exports = mongoose.model('Association', Association);