const swaggerJsdoc = require("swagger-jsdoc");
const func = require('./fn/function');

/**
/**
 * Définition des options Swagger
 */
//#region swagger
const swaggerOptions = {
  definition: {
      openapi: "3.0.0",
      info: {
        title: "Wallet Rest API",
        version: "1.0.0",
        description: "Simple Wallet management API.",
        contact: {
          name: "BankRob",
        },
        servers: ["http://localhost:" + func.normalizePort(process.env.PORT || '3000')],
      },
    },
    apis: ["router.js"], // files containing annotations as above
};

//#endregion

module.exports = swaggerJsdoc(swaggerOptions);