const express = require('express');
// eslint-disable-next-line new-cap
const router = express.Router();
const swaggerUi = require("swagger-ui-express");
const swaggerDocs = require("./swagger");
const serviceWallet = require('./service/WalletService');
const serviceOperation = require('./service/OperationService');

router.use("/api/v1/wallets/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));
//********** */
//  Wallet   */
//********** */
//#region Wallet

/**
 * @openapi
 * /api/v1/wallets:
 *   get:
 *     description: Returns all wallets in database.
 *     tags:
 *       - Wallet
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: ownerEmail
 *         type: string
 *         required: false
 *         description: If email (wallets?ownerEmail=johndoe@gmail.com) parameters then you search only the wallet with this address email.
 *       - in: query
 *         name: ownerId
 *         type: string
 *         required: false
 *         description: If Owner Id (wallets?ownerId='65465sdfsd4') parameters then search only the wallet's owner.
 *     responses:
 *       200:
 *         description: Returns (JSON) all wallets in database.
 *       204:
 *         description: Not wallet in database.
 *       503:
 *         description: Service Not Available
 */
 router.get('/api/v1/wallets', serviceWallet.getWallets);

/**
 * @openapi
 * /api/v1/wallets/{id}:
 *   get:
 *     description: To get user's wallet.
 *     tags:
 *       - Wallet
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *     responses:
 *       200:
 *         description: Return (JSON) wallet by his id.
 *       404:
 *         description: wallet with {id} does not exist.
 *       503:
 *         description: Service Not Available
 */
router.get('/api/v1/wallets/:id', (req, res, next)=>{
    if(req.url.startsWith('/api/v1/wallets/operations')) {
      next();
    } else {
        serviceWallet.getWalletById(req, res);
    }
  });

/**
 * @openapi
 * /api/v1/wallets:
 *   post:
 *     description: To create user's wallet.
 *     tags:
 *       - Wallet
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: ownerEmail and money are required.
 *             type: object
 *             required:
 *               - ownerEmail
 *               - money
 *             properties:
 *               ownerEmail:
 *                 type: string
 *               money:
 *                 type: string
 *               ownerId:
 *                 type: string
 *               ownerFirstName:
 *                 type: string
 *               ownerLastName:
 *                 type: string
 *     responses:
 *       201:
 *         description: Return (JSON) new Wallet.
 *       409:
 *         description: This Email Address already exists.
 *       503:
 *         description: Service Not Available
 */
router.post('/api/v1/wallets', serviceWallet.addWallet);

/**
 * @openapi
 * /api/v1/wallets:
 *   put:
 *     description: To modify user's wallet.
 *     tags:
 *       - Wallet
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: user's id is required.
 *             type: object
 *             required:
 *               - _id
 *             properties:
 *               _id: 
 *                 type: string
 *               ownerId:
 *                 type: string
 *               ownerLastName:
 *                 type: string
 *               ownerFirstName:
 *                 type: string
 *               ownerEmail:
 *                 type: string
 *               money:
 *                 type: string
 *     responses:
 *       200:
 *         description: wallet has been create.
 *       503:
 *         description: Service Not Available
 */
 router.put('/api/v1/wallets/update', serviceWallet.update);

/**
 * @openapi
 * /api/v1/wallets/{id}:
 *   delete:
 *     description: To delete user wallet.
 *     tags:
 *       - Wallet
 *     parameters:
 *       - in: path
 *         name: id
 *     responses:
 *       200:
 *         description: Wallet deleted successfully.
 *       404:
 *         description: Request failed ! Wallet with {id} not found.
 *       503:
 *         description: Service Not Available
 */
router.delete('/api/v1/wallets/:id', serviceWallet.delete);
//#endregion

//*********** */
//* OPERATION */
//*********** */
//#region Operation
/**
 * @openapi
 * /api/v1/wallets/operations:
 *   get:
 *     description: Returns all operations in database.
 *     tags:
 *       - Operation
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: owner
 *         type: string
 *         required: false
 *         description: If owner (wallets/operations?owner=johndoe@gmail.com) parameters then you search only the operation with this address owner.
 *       - in: query
 *         name: recipient
 *         type: string
 *         required: false
 *         description: If recipient (wallets/operations?recipient='65465sdfsd4') parameters then search only the wallet with this recipent.
 *     responses:
 *       200:
 *         description: Returns (JSON) all operations or specific operations relative to owner or recipient in database.
 *       204:
 *         description: Not operation in database.
 *       500:
 *         description: Internal Server Error
 */
router.get('/api/v1/wallets/operations', serviceOperation.getOperations);

/**
 * @openapi
 * /api/v1/wallets/operations/{id}:
 *   get:
 *     description: To get operation by his id.
 *     tags:
 *       - Operation
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: path
 *         name: id
 *     responses:
 *       200:
 *         description: Return (JSON) operation by his id.
 *       404:
 *         description: Operation with {id} does not exist..
 *       500:
 *         description: Internal Server Error
 */
router.get('/api/v1/wallets/operations/:id', serviceOperation.getOperationById);

/**
 * @openapi
 * /api/v1/wallets/operations:
 *   post:
 *     description: To create operation.
 *     tags:
 *       - Operation
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: date, amount, owner and recipient are required.
 *             type: object
 *             required:
 *               - date
 *               - amount
 *               - owner
 *               - recipient
 *             properties:
 *               date:
 *                 type: string
 *               amount:
 *                 type: string
 *               owner:
 *                 type: string
 *               recipient:
 *                 type: string
 *     responses:
 *       201:
 *         description: Return (JSON) new Wallet.
 *       500:
 *         description: Error message.
 */
 router.post('/api/v1/wallets/operations', serviceOperation.addOperation);
 
/**
 * @openapi
 * /api/v1/wallets/operations/{id}:
 *   delete:
 *     description: To delete operation.
 *     tags:
 *       - Operation
 *     parameters:
 *       - in: path
 *         name: id
 *     responses:
 *       200:
 *         description: Operation {id} has been deleted.
 *       404:
 *         description: Request failed ! Operation not found.
 *       500:
 *         description: Error message.
 */
 router.delete('/api/v1/wallets/operations/:id', serviceOperation.delete);
 //#endregion

router.use('/', (req, res, next) => {
    if (req.url == '/') {
        res.redirect('/api/v1/wallets/api-docs');
    } else {
        next();
    }
});

router.use((req, res) => {
    res.status(404).send('Page not found');
});

module.exports = router;