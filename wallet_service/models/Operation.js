const mongoose = require('mongoose');

const Operation = new mongoose.Schema({
    _id: {
        type: String,
        trim: true
    },
    date: {
        type: String,
        required: 'date is required'
    },
    amount: {
        type: String,
        trim: true,
        required: 'Amount is required'
    },
    owner: {
        type: String,
        trim: true,
        required: 'Owner is required'
    },
    recipient: {
        type: String,
        trim: true,
        required: 'Recipient is required'
    }
});

module.exports = mongoose.model('Operation', Operation);