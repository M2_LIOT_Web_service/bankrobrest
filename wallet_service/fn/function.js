module.exports = {
  normalizePort: (val) => {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
      // named pipe
      return val;
    }

    if (port >= 0) {
      // port number
      return port;
    }

    return false;
  },

  /**
   * Formalize JSON error message
   * @param {*} err
   */
  errorJson: (err) => {
    return {
      message: "Wallet API Request Error",
      error: err,
    };
  },
};
