const Operation = require("../models/Operation");
const Wallet = require("../models/Wallet");
const mongoose = require("mongoose");
const { errorJson } = require("../fn/function");

/**
 * Return all operations or operation by an owner or by a recipient
 * @param {*} req
 * @param {*} res
 */
exports.getOperations = async (req, res) => {
  let filter = {};
  // If recipient in request parameters
  if (req.query?.recipient) {
    filter = { recipient: req.query.recipient };
  }
  // If owner in request parameters
  if (req.query?.owner) {
    filter = { owner: req.query.owner };
  }
  Operation.find(filter)
    .exec()
    .then((operations) => {
      if (operations && operations.length > 0) {
        return res.status(200).json(operations);
      } else {
        return res.status(204).json("Not operation in database");
      }
    })
    .catch((err) => res.status(503).json(errorJson(err)));
};

/**
 * Find operation by his id
 * @param {*} req
 * @param {*} res
 * @returns
 */
exports.getOperationById = async (req, res) => {
  var _id = req.params.id;

  try {
    let operation = await Operation.findById({ _id: _id });
    if (operation) {
      return res.status(200).json(operation);
    } else {
      return res
        .status(404)
        .json(errorJson("Operation with id : " + _id + " does not exist."));
    }
  } catch (err) {
    return res.status(503).json(errorJson(err));
  }
};

exports.addOperation = async (req, res) => {
  const _id = mongoose.Types.ObjectId();
  const operation = new Operation({ _id: _id, ...req.body });
  try {
    const opSaveResult = await operation.save();

    if (opSaveResult._id) {
      const ownerParams = req.body.owner;
      const recipientParams = req.body.recipient;
      const amount = Number(req.body.amount);

      const walletOwner = await Wallet.findOne({
        $or: [{ ownerEmail: ownerParams }, { ownerId: ownerParams }],
      });

      if (!walletOwner) {
        await Operation.findOneAndDelete({ _id: opSaveResult._id});
        return res.status(404).json(errorJson("Owner does not exists ! Rollback operation"));
      }

      const walletRecipient = await Wallet.findOne({
        $or: [{ ownerEmail: recipientParams }, { ownerId: recipientParams }],
      });

      if (!walletRecipient ) {
        await Operation.findOneAndDelete({ _id: opSaveResult._id});
        return res.status(404).json(errorJson("Recipient does not exists ! Rollback operation"));
      }

      const newOwnerAccountValue = Number(walletOwner.money) - amount;
      walletOwner.money = newOwnerAccountValue.toString();

      const newRecipientAccountValue = Number(walletRecipient.money + amount);
      walletRecipient.money = newRecipientAccountValue.toString();

      let errorMessage = [];
      const walletOwnerSaveResult = await walletOwner.save();
      if(walletOwnerSaveResult._id === walletOwner._id){
        const walletRecipientSaveResult = await walletRecipient.save();
        if(walletRecipientSaveResult._id === walletRecipient._id){
          return res.status(201).json(`Operation saved successfully ! owner account : ${walletOwner.money} and recipient account : ${walletRecipient.money}`);
        } else {
          errorMessage.push("Save recipient failed ! operation rollback");
          walletRecipient.findOneAndDelete({ _id: walletRecipient._id });
        }
      } else {
          errorMessage.push("Save owner failed ! operation rollback");
          walletOwner.findOneAndDelete({ _id: walletOwner._id });
      }
      if (errorMessage.length > 0) {
        await Operation.findOneAndDelete({ _id: opSaveResult._id});
        return res.status(503).json(errorJson(errorMessage.join(", ")));
      }
    }
    throw "Save operation failed";
  } catch (err) {
    return res.status(503).json(err.message);
  }
  // operation.save((err) => {
  //   if (err) {
  //     console.log(err);
  //     return res.status(503).json(err);
  //   }
  //   const ownerParams = req.body.owner;
  //   const recipientParams = req.body.recipient;
  //   const amount = Number(req.body.amount);
  //   // { $or: [{ name: "Rambo" }, { breed: "Pugg" }, { age: 2 }] }
  //   const walletOwner = await Wallet.find({ $or: [ {ownerEmail: ownerParams}, {ownerId : ownerParams}] }).lean();
  //   if(!walletOwner.ownerEmail){
  //     return res.status(404).json(errorJson("Owner does not exists"));
  //   }
  //   const walletrecipient = await Wallet.find({ $or: [ {ownerEmail: recipientParams}, {ownerId : recipientParams}] }).lean();
  //   if(!walletrecipient.ownerEmail){
  //     return res.status(404).json(errorJson("Recipient does not exists"));
  //   }
  //   const newOwnerAccountValue = Number(walletOwner.money) - amount;
  //   const newRecipientAccountValue = Number(walletrecipient.money + amount);
  //   let errorMessage = [];
  //   walletOwner.save((err) => {
  //     if(err) errorMessage.push("Owner save error ")
  //   });
  //   walletrecipient.save((err) => {
  //     if(err) errorMessage.push("Recipient save error")
  //   });
  //   if(errorMessage.length > 0){
  //     return res.status(503).json(errorJson(errorMessage.join(", ")));
  //   }
  //   return res.status(201).json(operation);
  // });
};

exports.delete = async (req, res) => {
  Operation.findOneAndDelete({ _id: req.params.id })
    .exec()
    .then((result) => {
      if (result && result._id) {
        return res.status(200).json(`Operation ${result._id} has been deleted`);
      }
      return res
        .status(404)
        .json(errorJson("Request failed ! Operation not found."));
    })
    .catch((err) => res.status(503).json(errorJson(err)));
};
