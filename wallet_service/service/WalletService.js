const Wallet = require("../models/Wallet");
const mongoose = require("mongoose");
const {errorJson} = require('../fn/function');

/**
 * Return all wallets or wallet by owner email or owner id
 * @param {*} req
 * @param {*} res
 */
exports.getWallets = async (req, res) => {
  let filter = {};
  // If owner email in request parameters
  if (req.query.ownerEmail) {
    filter = { ownerEmail: req.query.ownerEmail };
  }
  // If owner id in request parameters
  if (req.query.ownerId) {
    filter = { ownerId: req.query.ownerId };
  }
  Wallet.find(filter)
    .exec()
    .then((wallets) => {
      if (wallets && wallets.length > 0) {
        return res.status(200).json(wallets);
      } else {
        return res.status(204).json("Not wallet in database");
      }
    })
    .catch((err) => res.status(503).json(errorJson(err)));
};

/**
 * Return wallet by his id
 * @param {*} req
 * @param {*} res
 * @returns
 */
exports.getWalletById = async (req, res) => {
  var _id = req.params.id;
  try {
    let wallet = await Wallet.findById({ _id: _id });
    if (wallet) {
      return res.status(200).json(wallet);
    } else {
      return res
        .status(404)
        .json(errorJson("wallet with id : " + _id + " does not exist."));
    }
  } catch (err) {
    return res.status(503).json(errorJson(err));
  }
};

/**
 * Create a wallet and add to database
 * @param {*} req 
 * @param {*} res 
 */
exports.addWallet = async (req, res) => {
  const _id = mongoose.Types.ObjectId();
  const wallet = new Wallet({ _id: _id, ...req.body });
  console.log(wallet);
  wallet.save((err) => {
    if (err) {
      console.log(err);
      if (err.code === "11000" || err.code === 11000) {
        return res.status(409).json("This Email Address or this OwnerId already exists");
      }
      return res.status(503).json(err);
    }
    return res.status(201).json(wallet);
  });
};

/**
 * Update a wallet by his id
 * @param {*} req 
 * @param {*} res 
 * @returns 
 */
exports.update = async (req, res) => {
  const _id = req.body.id;
  if (_id == undefined || _id == "") {
    return res.status(400).json(errorJson(`_id is required`));
  }
  try {
    const wallet = await Wallet.findById({ _id: _id });
    if(wallet){
        Wallet.findOneAndUpdate({ _id: _id }, result)
        .then((wallet) => {
          Wallet.findOne({ _id: _id })
            .then((newWallet) => res.status(200).json(newWallet) )
            .catch((err2) => res.status(500).json(errorJson(err2)) );
        })
        .catch((err) => {
          return res.status(503).json(errorJson(err));
        });
    }
  } catch (err) {
    return res.status(500).json(errorJson(err));
  }
};

exports.delete = async (req, res) => {
  const idWallet = req.params.id;
  try {
    // On supprime le groupe
    const result = await Wallet.findByIdAndDelete(idWallet);
    if (result && result._id) {
      // La suppression du wallet a réussi
      return res.status(200).json(`Wallet ${result._id} has been deleted`);

                // La suppression du portefeuille a réussi on supprime les opérations de ce portefeuille
                // const resAsso = await Association.deleteMany({id_group: idGroup});
                // let nbAssoDelete = "";
                // if(resAsso && resAsso.deletedCount){
                //     nbAssoDelete = resAsso.deletedCount;
                // }
                // return res.status(200).json(`Group ${result.name} has been deleted and ${nbAssoDelete} associations with this group`);
    }
    return res
      .status(404)
      .json(
        errorJson(`Request failed ! Wallet with id: ${idWallet} not found.`)
      );
  } catch (err) {
    return res.status(503).json(errorJson(errorJson(err)));
  }
};
