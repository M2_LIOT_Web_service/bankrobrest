const mongoose = require('mongoose');

const clientOptions = {
    socketTimeoutMS   : 30000,
    keepAlive         : true,
    useNewUrlParser   : true,
    useUnifiedTopology: true,
};

exports.InitDbConnection = async () => {
    try{
        await mongoose.connect(process.env.URL_WALLET_MONGO || 'mongodb://localhost:27019/', clientOptions);
        console.log('Connexion à mongo-wallet réussie');
    } catch (error) {
        console.log(`La connexion à mongo-wallet a échoué ${error}`);
        throw error;
    }
}