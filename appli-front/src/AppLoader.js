import React, { Component } from 'react'
import { MutatingDots } from  'react-loader-spinner';

export default class AppLoader extends Component {
    render() {
        return (
            <div className="d-flex flex-column h-100 position-absolute top-0 left-0 w-100 text-center align-items-center" style={{left:'0', backgroundColor:'#00000022', paddingTop:'30%'}}>
                <MutatingDots color="#00BFFF" height={120} width={120} />
                <h2>Loading...</h2>
            </div>
        )
    }
}
