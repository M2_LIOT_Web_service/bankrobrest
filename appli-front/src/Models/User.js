export class User {
    constructor(props) {
        this._id = props.id;
        this.name = props.name;
        this.firstName = props.firstName;
        this.email = props.email;
        this.password = props.password;
        this.data = props.data;
    }
}

export default User;
