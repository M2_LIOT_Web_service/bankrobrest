export const BASE_USER_URL = 'http://localhost:8000/api/v1'
export const USER_URL = 'http://localhost:8000/api/v1/users';
export const GROUP_URL = 'http://localhost:8020/api/v1/groups';
export const MAP_URL = 'http://localhost:8021/map';
export const NOTIFIER_BASE_URL = 'http://localhost:3001/api/v1/notifier';
export const NOTIFIER_EMAIL_URL = 'http://localhost:3001/api/v1/notifier/email';
