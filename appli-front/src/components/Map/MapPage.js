import React, { useState } from "react";
import { NavLink, Outlet } from "react-router-dom"; 
import "../subNavBar.css";

export default function Map() {
    const [activeLink, setActiveLink] = useState(0);

    const changeActiveLink = function(numLink) {
        setActiveLink(numLink);
    }

    return (
        <div>
            <div className="subNavBar">
                <NavLink onClick={() => changeActiveLink(0)} className={activeLink === 0 ? "subNav-link active" : "subNav-link"} to="/map/itinerary">
                    Itinéraire
                </NavLink> 
                <NavLink onClick={() => changeActiveLink(0)} className={activeLink === 0 ? "subNav-link active" : "subNav-link"} to="/map/itineraryfaster">
                    {/*itinéraire le plus court*/}
                </NavLink>
                <NavLink onClick={() => changeActiveLink(0)} className={activeLink === 0 ? "subNav-link active" : "subNav-link"} to="/map/itineraryFasterWithoutPlace">
                    {/*itinéraire le plus court*/}
                </NavLink> 
                <NavLink onClick={() => changeActiveLink(0)} className={activeLink === 0 ? "subNav-link active" : "subNav-link"} to="/map/itineraryFasterWithPlace">
                    {/*itinéraire le plus court*/}
                </NavLink> 
            </div>
            <Outlet />
        </div>
    );
}