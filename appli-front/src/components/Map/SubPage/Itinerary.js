import React from 'react'
import axios from "axios"

/**
 * startX, startY, endX, endY
 */

class Itinerary extends React.Component {
    constructor(props) {
        super(props);
        this.state = {xStart: 0, yStart: 0, xEnd: 0, yEnd: 0, address: "", travelTime: ""};

        this.handleXStartChange = this.handleXStartChange.bind(this);
        this.handleXEndChange = this.handleXEndChange.bind(this);
        this.handleYStartChange = this.handleYStartChange.bind(this);
        this.handleYEndChange = this.handleYEndChange.bind(this);

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleXStartChange(event) {
        this.setState({xStart: event.target.value});
    }

    handleYStartChange(event) {
        this.setState({yStart: event.target.value});
    }

    handleXEndChange(event) {
        this.setState({xEnd: event.target.value});
    }

    handleYEndChange(event) {
        this.setState({yEnd: event.target.value});
    }

    createItinerary = async() => {
        // Axios request
        const api = axios.create({
            baseURL: "http://localhost:8021/"
        })

        let res = await api.post("/api/v1/map/Itinerary", 
            { 
                xStart: this.state.xStart, 
                yStart: this.state.yStart, 
                xEnd: this.state.yStart, 
                yEnd: this.state.yEnd
            });

        console.log(res);
        this.setState({address: res.data.message.address, travelTime: res.data.message.travelTime});
    }

    handleSubmit(event) {
        this.createItinerary();
        event.preventDefault();
    }

    render() {
        return (<div className="container mt-5">
                     <h2>Récupérer l'itinéraire</h2>
                     <div className="row">
                         <div className="col-md-4">
                             <form onSubmit={this.handleSubmit}>
                                 {/* xStart, yStart */}
                                 <div className="mb-3">
                                     <label htmlFor="xStartInput" className="form-label">Coordonnée X de départ</label>
                                     <input type="number" value={this.state.xStart} onChange={this.handleXStartChange} className="form-control" id="xStartInput" />
                                 </div>
                                 <div className="mb-3">
                                     <label htmlFor="yStartInput" className="form-label">Coordonnée Y de départ</label>
                                     <input type="number" value={this.state.yStart} onChange={this.handleYStartChange} className="form-control" id="yStartInput" />
                                 </div>
                                 {/* xEnd, yEnd */}
                                 <div className="mb-3">
                                     <label htmlFor="xEndInput" className="form-label">Coordonnée X de d'arrivée</label>
                                     <input type="number" value={this.state.xEnd} onChange={this.handleXEndChange} className="form-control" id="xEndInput" />
                                 </div>
                                 <div className="mb-3">
                                     <label htmlFor="yEndInput" className="form-label">Coordonnée Y d'arrivée</label>
                                     <input type="number" value={this.state.yEnd} onChange={this.handleYEndChange} className="form-control" id="yEndInput" />
                                 </div>
                                 <input className='btn btn-primary' type="submit" value="Envoyer" />
                             </form>
                         </div>
                         <div className='col-md-6'>
                            <h3>Itinéraire finale</h3>
                            <ul className="list-group">
                                <li className="list-group-item">Adresse d'arrivée : {this.state.address}</li>
                                <li className="list-group-item">Temps de trajet : {this.state.travelTime}</li>
                            </ul>
                         </div>
                     </div>
                 </div>
            );
    }
}

export default Itinerary;
