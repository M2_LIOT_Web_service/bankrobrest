import React, { useContext, useState, useEffect } from "react";
import UserService from "../../Services/UserService";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

export default function Login() {

  const [errorLogin, setErrorlogin] = useState();
  const { isConnected } = useSelector(state => ({
    ...state.UserConnectedReducer
  }));
  const userConnectedDispatch = useDispatch();

  const navigate = useNavigate();
  useEffect(()=>{
    userConnectedDispatch({ 
      type: "SET_CONNECTED",
      payload: { isConnected: true }
    })
    console.log("test login", isConnected);
    if (localStorage.getItem("token") && localStorage.getItem("user")) {
      console.log("credentials ok");
      userConnectedDispatch({ 
        type: "SET_CONNECTED",
        payload: { isConnected: true }
      })
      return navigate("/user/allUser", {replace:true});
    }   

  },[]);

  const submit = async (e) => {
    e.preventDefault();
    const email = e.target[0].value;
    const password = e.target[1].value;
    let errorMessage = null;

    if (email && password) {
      try {
        const result = await UserService.login(email, password);
        if (result && result.status === 201) {
          console.log(result.data);
          localStorage.setItem("token", result.data.token);
          localStorage.setItem("user", {
            userName: result.data.userName,
            firstName: result.data.firstName
          });


          return navigate("/user/allUser", {replace:true});
        }
      }
      catch (err) {
        switch (err.response.status) {
          case 403:
            errorMessage = "Wrong credentials";
            break;
          case 404:
            errorMessage = "Wrong email or user not found";
            break;
        }
        console.log(err);
      }
    }
    setErrorlogin(errorMessage);
  }

  return (
    <div className="container">
      <div className="row centered-form">
        <div className="col-md-10 mx-auto p-0">
          <div className="panel panel-default mx-md-0 mx-sm-3 mx-lg-0 my-3">
            <div className="panel-heading">
              <h3 className="panel-title">Login</h3>
            </div>
            <div className="panel-body">
              {errorLogin ?
                <div className="error-login" style={{ color: 'red' }} id="error-login">{errorLogin}</div>
                :
                ''}
              <form method="post" onSubmit={submit}>
                <div className="form-group">
                  <input
                    type="email"
                    name="email"
                    pattern="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"
                    id="email"
                    className="form-control input-sm"
                    placeholder="Votre adresse Email"
                    required
                  />
                </div>
                <div className="form-group">
                  <input
                    type="password"
                    name="password"
                    id="password"
                    className="form-control input-sm"
                    placeholder="Mot de passe"
                    required
                  />
                </div>

                <button className="btn btn-info btn-block w-100 mb-2" type="submit">Connexion</button>
                {/* <button className="btn btn-info btn-block w-100 mb-2" onClick={this.submit}>Connexion</button> */}
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}