import React from "react";

export default function Register() {
  return (
    <div className="container">
      <div className="row centered-form">
        <div className="col-md-10 mx-auto p-0">
            <div className="panel panel-default mx-md-0 mx-sm-3 mx-lg-0 my-3">
                <div className="panel-heading">
                <h3 className="panel-title">
                    Inscrivez-vous
                </h3>
                </div>
                <div className="panel-body">
                    <form method="post">
                        <div className="row">
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <div className="form-group">
                            <input
                                type="text"
                                name="first_name"
                                id="first_name"
                                className="form-control input-sm"
                                placeholder="Nom"
                            />
                            </div>
                        </div>
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <div className="form-group">
                            <input
                                type="text"
                                name="last_name"
                                id="last_name"
                                className="form-control input-sm"
                                placeholder="Prénom"
                            />
                            </div>
                        </div>
                        </div>

                        <div className="form-group">
                        <input
                            type="email"
                            name="email"
                            id="email"
                            className="form-control input-sm"
                            placeholder="Votre adresse Email"
                        />
                        </div>

                        <div className="row">
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <div className="form-group">
                            <input
                                type="password"
                                name="password"
                                id="password"
                                className="form-control input-sm"
                                placeholder="Mot de passe"
                            />
                            </div>
                        </div>
                        <div className="col-xs-6 col-sm-6 col-md-6">
                            <div className="form-group">
                            <input
                                type="password"
                                name="password_confirmation"
                                id="password_confirmation"
                                className="form-control input-sm"
                                placeholder="Confirmez le mot de passe"
                            />
                            </div>
                        </div>
                        </div>

                        <input
                        type="submit"
                        value="Register"
                        className="btn btn-info btn-block w-100"
                        />
                    </form>
                </div>
            </div>
        </div>
      </div>
    </div>
  );
}
