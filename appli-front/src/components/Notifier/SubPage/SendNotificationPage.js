import React from "react";
import axios from "axios";

class SendNotification extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            title: "",
            message: "",
            result: ""
        }

        this.handleTitleChange = this.handleTitleChange.bind(this);
        this.handleMessageChange = this.handleMessageChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    
    handleTitleChange(event) {
        this.setState({title: event.target.value});
    }
    
    handleMessageChange(event) {
        this.setState({message: event.target.value});
    }

    sendNotificationAsync = async() => {
        const api = axios.create({
            baseURL: "http://localhost:8022/"
        })

        let res = await api.post("/api/v1/notifier", 
            { 
                title: this.state.title,
                message: this.state.message
            });

        console.log(res);
        this.setState({result: res.data});
    }

    handleSubmit(event) {
        this.sendNotificationAsync();
        event.preventDefault();
    }

    render() {
        return (
            <div className="container mt-5">
                <h2>Envoyer une notification</h2>
                <div className="row">
                    <div className="col-md-4">
                        <form onSubmit={this.handleSubmit}>
                            <div className="mb-3">
                                <label htmlFor="titleInput" className="form-label">Titre</label>
                                <input type="text" value={this.state.title} onChange={this.handleTitleChange} className="form-control" id="titleInput" />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="messageInput" className="form-label">Message</label>
                                <input type="text" value={this.state.message} onChange={this.handleMessageChange} className="form-control" id="messageInput" />
                            </div>
                            <input className='btn btn-primary' type="submit" value="Envoyer" />
                        </form>
                    </div>
                    <div className='col-md-6'>
                        <h3>Réponse de la requête</h3>
                        <ul className="list-group">
                            <li className="list-group-item">Réponse : {this.state.result}</li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default SendNotification;