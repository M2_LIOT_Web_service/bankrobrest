import React from "react";
import axios from "axios";

class SendEmail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            to: "",
            subject: "",
            message: "",
            result: ""
        }

        this.handleToChange = this.handleToChange.bind(this);
        this.handleSubjectChange = this.handleSubjectChange.bind(this);
        this.handleMessageChange = this.handleMessageChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    sendEmailAsync = async() => {
        const api = axios.create({
            baseURL: "http://localhost:8022/"
        })

        console.log("Send email");
        console.log("To: ", this.state.to);
        console.log("Subject: ", this.state.subject);
        console.log("Message: ", this.state.message);

        let res = await api.post("/api/v1/notifier/email", 
            { 
                to: this.state.to,
                subject: this.state.subject,
                text: this.state.message
            });
            
        console.log(res);
        this.setState({result: res.data});
    }

    handleSubmit(event) {
        this.sendEmailAsync();
        event.preventDefault();
    }

    handleToChange(event) {
        this.setState({to: event.target.value});
    }

    handleSubjectChange(event) {
        this.setState({subject: event.target.value});
    }

    handleMessageChange(event) {
        this.setState({message: event.target.value});
    }

    render() {
        return (
            <div className="container mt-5">
                <h2>Envoyer un e-mail</h2>
                <div className="row">
                    <div className="col-md-4">
                        <form onSubmit={this.handleSubmit}>
                            <div className="mb-3">
                                <label htmlFor="toInput" className="form-label">Destinataire</label>
                                <input type="text" value={this.state.to} onChange={this.handleToChange} className="form-control" id="toInput" />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="subjectInput" className="form-label">Sujet</label>
                                <input type="text" value={this.state.subject} onChange={this.handleSubjectChange} className="form-control" id="subjectInput" />
                            </div>
                            <div className="mb-3">
                                <label htmlFor="messageInput" className="form-label">Message</label>
                                <input type="text" value={this.state.message} onChange={this.handleMessageChange} className="form-control" id="messageInput" />
                            </div>
                            <input className='btn btn-primary' type="submit" value="Envoyer" />
                        </form>
                    </div>
                    <div className='col-md-6'>
                        <h3>Réponse de la requête</h3>
                        <ul className="list-group">
                            <li className="list-group-item">Réponse : {this.state.result}</li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
}

export default SendEmail;