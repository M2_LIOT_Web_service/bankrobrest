import React, { useState } from "react";
import { NavLink, Outlet } from "react-router-dom"; 
import "../subNavBar.css";

export default function Notifier() {
    const [activeLink, setActiveLink] = useState(0);

    const changeActiveLink = function(numLink) {
        setActiveLink(numLink);
    }

    return (
        <div>
            <div className="subNavBar">
                <NavLink onClick={() => changeActiveLink(0)} className={activeLink === 0 ? "subNav-link active" : "subNav-link"} to="/notifier/sendnotification">
                    Envoyer une notification
                </NavLink> 
                <NavLink onClick={() => changeActiveLink(0)} className={activeLink === 0 ? "subNav-link active" : "subNav-link"} to="/notifier/sendemail">
                    Envoyer un email
                </NavLink>
            </div>
            <Outlet />
        </div>
    );
}