import React, {useState, useContext} from 'react';
import './Header.css';
//import {useNavigate} from 'react-router-dom';
import { NavLink } from 'react-router-dom';

function Header(){
    //const navigate = useNavigate();
    const [userConnected, setUserConnected] = useState();
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark justify-content-between">
                <h1 className="logo ps-3">BankRob</h1>
                <ul className="navbar-nav mr-auto pe-5">
                    <li className="nav-item active">
                        <NavLink className="nav-link" to="/user/allUser">Utilisateur</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/group">Groupe</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/map/itinerary">Map</NavLink>
                    </li>
                    <li className="nav-item">
                        {/* <a href="#" className="nav-link">
                            Itinéraire
                        </a> */}
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link" to="/notifier">Notification</NavLink>
                    </li>
                    <li className="nav-item">
                        {/* <a href="#" className="nav-link">
                            Portefeuille
                        </a> */}
                    </li>
                </ul>
            </nav>
        </div>
    )
}

export default Header;