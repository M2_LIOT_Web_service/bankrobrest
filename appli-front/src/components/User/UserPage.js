import React, {useState} from "react";
import { NavLink, Outlet } from "react-router-dom";
import "../subNavBar.css";

export default function User() {
    const [activeLink, setActiveLink] = useState(0);

    const changeActiveLink = function(numLink){
        setActiveLink(numLink);
    }

  return (
    <div>
      <nav className="subNavBar">
        <NavLink onClick={() => changeActiveLink(0)} className={activeLink === 0 ? "subNav-link active" : "subNav-link"} to="/user/allUser">
          Tous les utilisateurs
        </NavLink>
        <NavLink onClick={() => changeActiveLink(1)} className={activeLink === 1 ? "subNav-link active" : "subNav-link"} to="/user/addUser">
          Ajouter un utilisateur
        </NavLink>
        <NavLink onClick={() => changeActiveLink(2)} className={activeLink === 2 ? "subNav-link active" : "subNav-link"} to="/user/deleteUser">
          Supprimer un utilisateur
        </NavLink>
      </nav>
      <Outlet />
    </div>
  );
}
