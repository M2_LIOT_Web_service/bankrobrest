import React, { useState, useEffect } from "react";
import UserService from "../../../Services/UserService";
import UserCard from "../../../Widget/User/UserCard";
import { useNavigate } from "react-router-dom";

export default function AllUser() {
  let navigate = useNavigate();
  const [userList, setUserList] = useState([]);

  const getUsers = async function () {
    const users = await UserService.getUsers();
    if (users && users.error) {
      return navigate("/login");
    }
    console.log("users : ", users);
    setUserList(users);
  };

  //   useEffect(() => {
  //     getUsers();
  //   }, []);

  return (
    <div className="container">
      <h2 className="pb-3">Listes des utilisateurs</h2>
      <button className="btn btn-primary mb-3" onClick={getUsers}>
        Rafraîchir les données
      </button>
      <div className="user-card-container">
        {userList.map((user, index) => {
          return <UserCard key={index} {...user} />;
        })}
      </div>
    </div>
  );
}
