import React, { useState } from 'react'
import UserService from '../../../Services/UserService';

export default function AddUser() {
    const [infoMessage, setInfoMessage] = useState();
    const submit = async (e) => {
        e.preventDefault();
        // setInfoMessage(null);
        try {
            const result = await UserService.addUser({
                userName: e.target[0].value,
                firstName: e.target[1].value,
                email: e.target[2].value,
                data: e.target[3].value,
                password: e.target[4].value
            });
            if(result.status === 201){
                setInfoMessage({
                    class: "alert-success text-primary",
                    message: `L'utilisateur avec l'adresse mail ${e.target[2].value} a été ajouté !`,
                  });
            }
        } catch (err) {
            setInfoMessage({
                class: "alert-danger text-danger",
                message: "Il semble que l'utilisateur existe déjà",
              });
        }
    }

    return (
        <div className="container">
            <h2 className="pb-3">Ajouter un utilisateur</h2>
            {infoMessage && infoMessage.message != "" ? (
                <div className={`alert ${infoMessage.class}`}>
                    <h3>{infoMessage.message}</h3>
                </div>
            ) : (
                ""
            )}
            <form onSubmit={submit}>
                <div className="mb-3">
                    <label htmlFor="nameInput" className="form-label">Nom</label>
                    <input type="text" className="form-control" id="nameInput" required />
                </div>
                <div className="mb-3">
                    <label htmlFor="firstnameInput" className="form-label">Prénom</label>
                    <input type="text" className="form-control" id="firstnameInput" required />
                </div>
                <div className="mb-3">
                    <label htmlFor="emailInput" className="form-label">Email</label>
                    <input type="email" className="form-control" id="emailInput" pattern="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$" required />
                </div>
                <div className="mb-3">
                    <label htmlFor="dataInput" className="form-label">Données supplémentaires</label>
                    <textarea className="form-control" id="dataInput" rows="3"></textarea>
                </div>
                <div className="mb-3">
                    <label htmlFor="inputPassword1" className="form-label">Password</label>
                    <input type="password" className="form-control" id="inputPassword1" required />
                </div>
                <button type="submit" className="btn btn-primary">Envoyer la requête</button>
            </form>
        </div>
    )
}
