import React, { useState } from "react";
import UserService from "../../../Services/UserService";
import { useNavigate } from "react-router-dom";

export default function DeleteUser() {
  const [infoMessage, setInfoMessage] = useState();
  const navigate = useNavigate();
  const submit = async (e) => {
    e.preventDefault();
    try {
      const id = e.target[0].value;
      if (id) {
        const result = await UserService.deleteUser(id);
        // UserStore.delete(id);
        let status = 200;
        if (result.status !== 200) {
          status = parseInt(result?.response?.status);
        }
        console.log(result.response);
        switch (status) {
          case 200:
            let mess = result.data.slice(5);
            mess =
              "l'utilisateur " +
              mess.slice(0, mess.indexOf("has")) +
              " a été supprimé !";
            setInfoMessage({
              class: "alert-success text-primary",
              message: mess,
            });
            break;
          case 401, 498:
            setInfoMessage({
              class: "alert-danger text-danger",
              message: "Vous devez être authentifié. Vous allez être dirigé vers la page de login.",
            });
            setTimeout(() => {
              localStorage.removeItem("token");
              localStorage.removeItem("user");
              navigate("/login");
            }, 2000);
            break;
          case 403:
            setInfoMessage({
              class: "alert-danger text-danger",
              message: "Vous n'avez pas les accès nécessaire pour cette ressource",
            });
            break;
          case 404:
            setInfoMessage({
              class: "alert-danger text-danger",
              message: "L'utilisateur n'existe pas !",
            });
            break;
          default:
            setInfoMessage({
              class: "alert-danger text-danger",
              message: "Une erreur est survenue !",
            });
        }
      } else {
        setInfoMessage({
          class: "alert-danger text-danger",
          message: "Vous devez saisir un identifiant !",
        });
      }
    } catch (err) {
      setInfoMessage({
        class: "alert-danger text-danger",
        message: err.message,
      });
    }
  };

  return (
    <div className="container">
      <h2 className="pb-3">Supprimer un utilisateur</h2>
      {infoMessage && infoMessage.message != "" ? (
        <div className={`alert ${infoMessage.class}`}>
          <h3>{infoMessage.message}</h3>
        </div>
      ) : (
        ""
      )}

      <form onSubmit={submit}>
        <div className="mb-3">
          <label htmlFor="idInput" className="form-label">
            Identifiant de l'utilisateur
          </label>
          <input type="text" className="form-control" id="idInput" />
        </div>
        <button type="submit" className="btn btn-primary">
          Envoyer la requête
        </button>
      </form>
    </div>
  );
}
