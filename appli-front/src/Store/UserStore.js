import React from 'react';
class UserStore extends React.Component {
    static users = [];
    static lastUpdate;
    
    static tokenFromHeaders(headers) {
        const header = headers['authorization'] || headers['Authorization'];
        this.token = header.slice(7, header.length);
        console.log('this.token : ',this.token);
        return this.token;
    }

    static copyUserArray(userArray){
        this.users = [...userArray];
        this.sortUsers();
    }

    static add(user){
        this.users.push(user);
        this.sortUsers();
    }

    static replace(user){
        let index = 0;
        let end = false;
        while (!end){
            if(this.users[index].email === user.emai ){
                this.users[index] = user;
                end = true;
            }
            index++;
        }
        this.sortUsers();
    }

    static delete(id){
        this.users = this.users.filter(e => e._id !== id);
    }

    static sortUsers(){
        this.users.sort((a,b) => {
            let nameA = a.userName.toLowerCase();
            let nameB = b.userName.toLowerCase();
            if (nameA < nameB) {return -1;}
            if (nameA > nameB) {return 1;}
            return 0;
        });
    }
}

export default UserStore;