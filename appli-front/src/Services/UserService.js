import axios from "axios";
import { BASE_USER_URL, USER_URL } from "../Url";
import userStore from "../Store/UserStore";

/**
 * Description du service : Fais la gestion du compte de l’utilisateur 
 * (mot de passe, gestion du pseudo, contact utilisateur, compétences, authentification)
 */
class UserService {

    static tokenPayload() {
        const config = {
            headers: {
                'Authorization': 'Bearer ' + localStorage.getItem('token')
            }
        }
        return config;
    }

    static async getUsers() {
        try {
            const result = await axios.get(`${USER_URL}`, this.tokenPayload());
            console.log("get users service: ", result);
            userStore.copyUserArray(result.data);
            userStore.lastUpdate = Date.now;
            userStore.tokenFromHeaders(result.headers);
            localStorage.setItem('token', userStore.token);
            return userStore.users;
        } catch (err) {
            if (err.response) {
                console.log("get users service: ", err.response.data);
                alert(err.response.data);
                localStorage.removeItem('token', "");
                localStorage.removeItem('user', "");
                return { error: err.response.data };
            }
            return { error: 'Cannot connect with user server' };
        }
    }

    static async addUser(user) {
        const result = await axios.post(`${USER_URL}`, user);
        // UserStore.add(user);
        console.log("user service add User : ", result);
        return result;
    }

    static async deleteUser(id) {
        try {
            const result = await axios.delete(`${USER_URL}/${id}`, this.tokenPayload());
            localStorage.setItem('token', userStore.tokenFromHeaders(result.headers));
            return result;
        } catch (err) {
            if (err.response.headers.authorization){
                localStorage.setItem('token', userStore.tokenFromHeaders(err.response.headers));
            }
            return err;
        }
    }

    static async login(email, password) {
        const result = await axios.post(`${BASE_USER_URL}/login`, { email: email, password: password });
        console.log("delete token : ", localStorage.getItem('token'))
        return result;
    }
}

export default UserService;



