import React from 'react';
import './userCard.css';

export default function UserCard(props) {
    return (
        <div className="user-card">
            <h5>{props.userName + ' ' + props.firstName} </h5>
            <p><small style={{fontsize: '10px'}}>Id : {props._id}</small></p>
            <p>{props.email}</p>
            { props.data && <p className="user-card-data">{props.data.carac.join(', ')}</p> }
        </div>
    )
}
