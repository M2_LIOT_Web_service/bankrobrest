import { createStore, combineReducers, applyMiddleware } from "redux";
import UserConnectedReducer from "./Reducers/UserConnectedReducer";
import thunk from "redux-thunk"; // thunk permet de faire passer des fonction au middleware

const rootReducer = combineReducers({
  UserConnectedReducer,
})

const customMiddleware = store => next => action => {
    console.log(store);
    console.log(next);
    console.log(action);
}

const Store = createStore(rootReducer, applyMiddleware(customMiddleware));
//const Store = createStore(rootReducer, applyMiddleware(thunk));
// dans le reducer ajouter les fonctions

export default Store;