const INITIAL_STATE = {
    isConnected: false
}

function UserConnectedReducer(state = INITIAL_STATE, action){
    switch(action.type){
        case 'SET_CONNECTED': {
            return {
                ...state,
                isConnected: action.payload.isConnected
            }
        }
    }
    return state;
}

export default UserConnectedReducer;

// fonction si thunk est utilisé
export const getConnected = () => dispatch => {
    dispatch({
        type: "SET_CONNECTED",
        payload: { isConnected: true }
    })
}

// pour utiliser la fonction dispatch(getConnected())