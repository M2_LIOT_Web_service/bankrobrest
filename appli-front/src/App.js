import React, { useState, useEffect } from "react";
import Header from "./components/Header/Header";
import { Routes, Route } from "react-router-dom";
import UserPage from "./components/User/UserPage";
import Group from "./components/Group/GroupPage";
import Map from "./components/Map/MapPage";
import AllUser from "./components/User/SubPage/AllUser";
import AddUser from "./components/User/SubPage/AddUser";
import DeleteUser from "./components/User/SubPage/DeleteUser";
import Itinerary from "./components/Map/SubPage/Itinerary";
import ItineraryFaster from "./components/Map/SubPage/ItineraryFaster";
import ItineraryFasterWithPlace from "./components/Map/SubPage/ItineraryFasterWithPlace";
import ItineraryFasterWithoutPlace from "./components/Map/SubPage/ItineraryFasterWithoutPlace";
import Notifier from "./components/Notifier/NotifierPage";
import SendNotification from "./components/Notifier/SubPage/SendNotificationPage";
import SendEmail from "./components/Notifier/SubPage/SendEmailPage";
import Register from "./components/Auth/Register";
import Login from "./components/Auth/Login";
import axios from "axios";
import AppLoader from "./AppLoader";

function App() {
  const [loader, setLoader] = useState(false);

  useEffect(() => {
    axios.interceptors.request.use(
      function (config) {
        setLoader(true);
        return config;
      },
      function (error) {
        return Promise.reject(error);
      }
    );

    axios.interceptors.response.use(
      function (response) {
        setLoader(false);
        return response;
      },
      function (error) {
        setLoader(false);
        return Promise.reject(error);
      }
    );

    localStorage.removeItem("token");
    localStorage.removeItem("user");
  }, []);

  return (
    <div className="App">
      <Header />
      {loader ? <AppLoader /> : ""}
      <Routes>
        {/* <Route exact path="/" component={Home}/> */}
        <Route path="/register" element={<Register />} />
        <Route path="/login" element={<Login />} />
        <Route path="/user" element={<UserPage />}>
          <Route path="/user/allUser" element={<AllUser />} />
          <Route path="/user/addUser" element={<AddUser />} />
          <Route path="/user/deleteUser" element={<DeleteUser />} />
        </Route>
        <Route path="/group" element={<Group />} />
        <Route path="/map" element={<Map />}>
          <Route path="/map/itinerary" element={<Itinerary />} />
          <Route path="/map/itineraryfaster" element={<ItineraryFaster />} />
          <Route
            path="/map/itineraryfasterwithplace"
            element={<ItineraryFasterWithPlace />}
          />
          <Route
            path="/map/itineraryfasterwithoutplace"
            element={<ItineraryFasterWithoutPlace />}
          />
        </Route>
        <Route path="/notifier" element={<Notifier />}>
          <Route
            path="/notifier/sendnotification"
            element={<SendNotification />}
          />
          <Route path="/notifier/sendemail" element={<SendEmail />} />
        </Route>
        <Route path="*" element={<Login />} />
      </Routes>
    </div>
  );
}

export default App;
