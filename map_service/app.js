const express = require('express');
const app = express();
const morgan = require('morgan');
const func = require('./fn/function');
const router = require('./router');
const cors = require('cors');

app.use(cors({
    exposedHeaders: ['Authorization'],
    origin: '*'
}));
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', router);

app.listen(func.normalizePort(process.env.MAP_PORT || '3000'), () =>{
    console.log('server map initialized');
});



