const express = require('express');
const router = express.Router();
const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require('swagger-ui-express');
const service = require('./service/mapService');
const func = require('./fn/function');

/**
 * Définition des options Swagger
 */
//#region swagger
const swaggerOptions = {
    definition: {
        openapi: "3.0.0",
        info: {
          title: "Map Rest API",
          version: "1.0.0",
          description: "Simple Map management API.",
          contact: {
            name: "BankRob",
          },
          servers: ["http://localhost:" + func.normalizePort(process.env.MAP_PORT || '3000')],
        },
      },
      apis: ["router.js"], // files containing annotations as above
};
const swaggerDocs = swaggerJsdoc(swaggerOptions);
//#endregion

router.use("/api/v1/map/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

/**
 * @openapi
 * /api/v1/map/itinerary:
 *   post:
 *     description: To create an itinerary.
 *     tags:
 *       - Itinerary
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: departure and arrival coordinates are required.
 *             type: object
 *             required:
 *               - xStart
 *               - yStart
 *               - xEnd
 *               - yEnd
 *             properties:
 *               xStart:
 *                 type: number
 *               yStart:
 *                 type: number
 *               xEnd:
 *                 type: number
 *               yEnd:
 *                 type: number
 *     responses:
 *       500:
 *          description: Error.
 *       201:
 *         description: Return  "itinerary is create".
 */
router.post("/api/v1/map/itinerary", service.itinerary);

/**
 * @openapi
 * /api/v1/map/itineraryFaster:
 *   post:
 *     description: To create the faster itinerary.
 *     tags:
 *       - Itinerary
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: departure and arrival coordinates are required.
 *             type: object
 *             required:
 *               - xStart
 *               - yStart
 *               - xEnd
 *               - yEnd
 *             properties:
 *               xStart:
 *                 type: number
 *               yStart:
 *                 type: number
 *               xEnd:
 *                 type: number
 *               yEnd:
 *                 type: number
 *     responses:
 *       500:
 *          description: Error.
 *       201:
 *         description: Return  "itinerary is create".
 */
router.post("/api/v1/map/itineraryFaster", service.itineraryFaster);

/**
 * @openapi
 * /api/v1/map/itineraryWithoutPlace:
 *   post:
 *     description: To create the itinerary avoiding places.
 *     tags:
 *       - Itinerary
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: departure and arrival coordinates are required.
 *             type: object
 *             required:
 *               - xStart
 *               - yStart
 *               - xEnd
 *               - yEnd
 *             properties:
 *               xStart:
 *                 type: number
 *               yStart:
 *                 type: number
 *               xEnd:
 *                 type: number
 *               yEnd:
 *                 type: number
 *     responses:
 *       500:
 *          description: Error.
 *       201:
 *         description: Return  "itinerary is create".
 */
router.post("/api/v1/map/itineraryWithoutPlace", service.itineraryWithoutPlace);

/**
 * @openapi
 * /api/v1/map/itineraryFasterWithoutPlace:
 *   post:
 *     description: To create the faster itinerary avoiding places.
 *     tags:
 *       - Itinerary
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: departure and arrival coordinates are required.
 *             type: object
 *             required:
 *               - xStart
 *               - yStart
 *               - xEnd
 *               - yEnd
 *             properties:
 *               xStart:
 *                 type: number
 *               yStart:
 *                 type: number
 *               xEnd:
 *                 type: number
 *               yEnd:
 *                 type: number
 *     responses:
 *       500:
 *          description: Error.
 *       201:
 *         description: Return  "itinerary is create".
 */
router.post("/api/v1/map/itineraryFasterWithoutPlace", service.itineraryFasterWithoutPlace);

/**
 * @openapi
 * /api/v1/map/itineraryWithPlace:
 *   post:
 *     description: To create the itinerary through places.
 *     tags:
 *       - Itinerary
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: departure and arrival coordinates are required.
 *             type: object
 *             required:
 *               - xStart
 *               - yStart
 *               - xEnd
 *               - yEnd
 *             properties:
 *               xStart:
 *                 type: number
 *               yStart:
 *                 type: number
 *               xEnd:
 *                 type: number
 *               yEnd:
 *                 type: number
 *     responses:
 *       500:
 *          description: Error.
 *       201:
 *         description: Return  "itinerary is create".
 */
router.post("/api/v1/map/itineraryWithPlace", service.itineraryWithPlace);

/**
 * @openapi
 * /api/v1/map/itineraryFasterWithPlace:
 *   post:
 *     description: To create the faster itinerary through places.
 *     tags:
 *       - Itinerary
 *     produces:
 *       - application/json
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             description: departure and arrival coordinates are required.
 *             type: object
 *             required:
 *               - xStart
 *               - yStart
 *               - xEnd
 *               - yEnd
 *             properties:
 *               xStart:
 *                 type: number
 *               yStart:
 *                 type: number
 *               xEnd:
 *                 type: number
 *               yEnd:
 *                 type: number
 *     responses:
 *       500:
 *          description: Error.
 *       201:
 *         description: Return  "itinerary is create".
 */
router.post("/api/v1/map/itineraryFasterWithPlace", service.itineraryFasterWithPlace);

router.use('/', (req, res, next) => {
    if (req.url == '/') {
        res.redirect('/api/v1/map/api-docs');
    } else {
        next();
    }
});

router.use((req, res) => {
    res.status(404).send('Page not found');
});

module.exports = router;