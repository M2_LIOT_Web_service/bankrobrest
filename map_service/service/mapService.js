const { place } = require('../models/contactDetailsPlace');
const ContactDetailsPlace = require('../models/contactDetailsPlace');
const ContactDetailsXY = require('../models/contactDetailsXY');

// -------
// Description du service : Ce service permet de récupérer un itinéraire entre 2 points en évitant les commissariats 
// et gendarmerie ou un temps d’arrivée des forces de l’ordre à partir d’un point.
// -------

const addressList = ["30, rue Munoz Albert", "814, place Inès Becker Meyer", "9, place de Brun Benard-sur-Mer", "330, impasse Benoît Pires Auger", "83 Rue Bertrand Renault Bilen"];
const timeList = ["1 heure", "45 minutes", "5 heures et 30 minutes", "6 heures et 15 minutes", "3 heures"];

/**
 * @summary Vérifie si les coordonnées sont bien présentes
 * @param {json} contactDetailsXY Contient les coordonnées
 * @returns {boolean} Si les coordonnées sont présentes, on retourne true, dans le cas contraire, on retourne false
 */
function CheckContactDetailsXY(contactDetailsXY) {
    if (contactDetailsXY.xStart != 0 && contactDetailsXY.yStart != 0 
        && contactDetailsXY.yStart != 0 && contactDetailsXY.yEnd != 0) {
        return true;            
    } 
    return false;
}

/**
 * @summary Vérifie si les adresses sont bien présentes
 * @param {json} contactDetailsPlace Contient les adresses de départ et d'arrivée
 * @returns {boolean} Si les adresses sont présentes, on retourne true, dans le cas contraire, on retourne false
 */
function CheckContactDetailsWithoutPlace(contactDetailsPlace) {
    if (contactDetailsPlace.startAddress != "" && contactDetailsPlace.endAddress != "") {
        return true;            
    } 
    return false;
}

/**
 * @summary Vérifie si les adresses sont bien présentes avec le nom du lieux à éviter
 * @param {json} contactDetailsPlace Contient les adresses de départ et d'arrivée
 * @returns {boolean} Si les adresses sont présentes, on retourne true, dans le cas contraire, on retourne false
 */
function CheckContactDetailsWithPlace(contactDetailsPlace) {
    if (contactDetailsPlace.startAddress != "" && contactDetailsPlace.endAddress != "", contactDetailsPlace.place != "") {
        return true;            
    } 
    return false;
}

/**
 * Format json message
 */
jsonMessage = (msg) => {
    return {
        message: msg
    }
}

/**
 * Format json message for address and travel time
 */
jsonAddressAndTravelTime = (address, travelTime) => {
    return {
        message: {
            address: address,
            travelTime: travelTime
        }
    }
}

// Exemple de POST pour la méthode ci-dessous
// ----------------------------------------------------
// {"xStart": 20, "yStart": 25, "xEnd": 23, "yEnd": 42}
// ----------------------------------------------------
/**
 * @summary Permet de trouver l'itinéraire
 * @param {json} req Coordonnées de départ et coordonnées d'arrivée
 * @param {json} res Statut 200 si la réponse est trouvée et statut 500 dans le cas contraire
 * @returns {statut} Le statut associé
 */
exports.itinerary = async(req, res) => {
    ContactDetailsXY.xStart = req.body.xStart;
    ContactDetailsXY.xEnd = req.body.yStart;
    ContactDetailsXY.yStart = req.body.xEnd;
    ContactDetailsXY.yEnd = req.body.yEnd;
    
    var isValid = CheckContactDetailsXY(ContactDetailsXY);

    if (!isValid) {
        res.sendStatus(500);
    } else {
        console.log("--------------------");
        console.log("Method Get itinerary");
        console.log("--------------------");

        const random = Math.floor(Math.random() * addressList.length);
        const randAddress = addressList[random];
        const randTime = timeList[random];

        res.status(201).send(jsonAddressAndTravelTime(randAddress, randTime));
    }
}

// Exemple de POST pour la méthode ci-dessous
// ----------------------------------------------------
// {"xStart": 20, "yStart": 25, "xEnd": 23, "yEnd": 42}
// ----------------------------------------------------
/**
 * @summary Permet de trouver l'itinéraire le plus rapide
 * @param {json} req Coordonnées de départ et coordonnées d'arrivée
 * @param {json} res Statut 200 si la réponse est trouvée et statut 500 dans le cas contraire
 * @returns {statut} Le statut associé
 */
exports.itineraryFaster = async(req, res) => {
    ContactDetailsXY.xStart = req.body.xStart;
    ContactDetailsXY.xEnd = req.body.yStart;
    ContactDetailsXY.yStart = req.body.xEnd;
    ContactDetailsXY.yEnd = req.body.yEnd;
    
    var isValid = CheckContactDetailsXY(ContactDetailsXY);

    if (!isValid) {
        res.sendStatus(500);
    } else {
        console.log("--------------------");
        console.log("Method Get itinerary faster");
        console.log("--------------------");
        var message = "xStart: " + ContactDetailsXY.xStart + "; yStart: " + ContactDetailsXY.yStart + "; xEnd: " + ContactDetailsXY.xEnd + "; yEnd: " + ContactDetailsXY.yEnd;
        console.log(message);
        res.status(201).send(jsonMessage(message));
    }
}

// Exemple de POST pour la méthode ci-dessous
// ----------------------------------------------------
// {"startAddress": "7 rue de Stalingrad 31000 Toulouse", "endAddress": "80 rue des amidonniers 31000 Toulouse"}
// ----------------------------------------------------
/**
 * @summary Permet d’avoir l’itinéraire sans passer par des lieux comme gendarmerie, commissariat, école, hôpital
 * @param {json} req Adresse de départ et adresse d'arrivée
 * @param {json} res Statut 200 si la réponse est trouvée et statut 500 dans le cas contraire
 * @returns {statut} Le statut associé
 */
exports.itineraryWithoutPlace = async(req, res) => {
    ContactDetailsPlace.startAddress = req.body.startAddress;
    ContactDetailsPlace.endAddress = req.body.endAddress;

    var isValid = CheckContactDetailsWithoutPlace(ContactDetailsPlace);

    if (!isValid) {
        res.sendStatus(500);
    } else {
        console.log("--------------------");
        console.log("Method Get itinerary without place");
        console.log("--------------------");
        var message = "Start address: " + ContactDetailsPlace.startAddress + "; End address: " + ContactDetailsPlace.endAddress;
        console.log(message);
        
        res.status(201).send(jsonMessage(message));
    }
}

// POST body example
// ----------------------------------------------------
// {"startAddress": "7 rue de Stalingrad 31000 Toulouse", "endAddress": "80 rue des amidonniers 31000 Toulouse"}
// ----------------------------------------------------
/**
 * @summary Permet d’avoir l’itinéraire le plus court sans passer par des lieux comme gendarmerie, commissariat, école, hôpital
 * @param {json} req Adresse de départ et adresse d'arrivée
 * @param {json} res Statut 200 si la réponse est trouvée et statut 500 dans le cas contraire
 * @returns {statut} Le statut associé
 */
exports.itineraryFasterWithoutPlace = async(req, res) => {
    ContactDetailsPlace.startAddress = req.body.startAddress;
    ContactDetailsPlace.endAddress = req.body.endAddress;
    
    var isValid = CheckContactDetailsWithoutPlace(ContactDetailsPlace);

    if (!isValid) {
        res.sendStatus(500);
    } else {
        console.log("--------------------");
        console.log("Method Get itinerary faster without place");
        console.log("--------------------");
        var message = "Start address: " + ContactDetailsPlace.startAddress + "; End address: " + ContactDetailsPlace.endAddress;        
        res.status(201).send(jsonMessage(message));
    }
}

// Exemple de POST pour la méthode ci-dessous
// ----------------------------------------------------
// {"place": "Commissariat Toulouse", "startAddress": "7 rue de Stalingrad 31000 Toulouse", "endAddress": "80 rue des amidonniers 31000 Toulouse"}
// ----------------------------------------------------
/**
 * @summary Permet d’avoir l’itinéraire en évitant un lieu indiqué
 * @param {json} req Addresse de départ, adresse d'arrivée et lieu par lequel il ne faut pas passer
 * @param {json} res Statut 200 si la réponse est trouvée et statut 500 dans le cas contraire
 * @returns {statut} Le statut associé
 */
exports.itineraryWithPlace = async(req, res) => {
    ContactDetailsPlace.startAddress = req.body.startAddress;
    ContactDetailsPlace.endAddress = req.body.endAddress;
    ContactDetailsPlace.place = req.body.place;

    var isValid = CheckContactDetailsWithPlace(ContactDetailsPlace);

    if (!isValid) {
        res.sendStatus(500);
    } else {
        console.log("--------------------");
        console.log("Method Get itinerary with place");
        console.log("--------------------");

        var message = "Place: " + ContactDetailsPlace.place + "; Start address: " + ContactDetailsPlace.startAddress + "; End address: " + ContactDetailsPlace.endAddress;
        res.status(201).send(jsonMessage(message));
    }
}

// Exemple de POST pour la méthode ci-dessous
// ----------------------------------------------------
// {"place": "My place name", "startAddress": "7 rue de Stalingrad 31000 Toulouse", "endAddress": "80 rue des amidonniers 31000 Toulouse"}
// ----------------------------------------------------
/**
 * @summary Permet d’avoir l’itinéraire le plus court en évitant un lieu indiqué
 * @param {json} req Addresse de départ, adresse d'arrivée et lieu par lequel il ne faut pas passer
 * @param {json} res Statut 200 si la réponse est trouvée et statut 500 dans le cas contraire
 * @returns {statut} Le statut associé
 */
exports.itineraryFasterWithPlace = async(req, res) => {
    ContactDetailsPlace.startAddress = req.body.startAddress;
    ContactDetailsPlace.endAddress = req.body.endAddress;
    ContactDetailsPlace.place = req.body.place;

    var isValid = CheckContactDetailsWithPlace(ContactDetailsPlace);

    if (!isValid) {
        res.sendStatus(500);
    } else {
        console.log("--------------------");
        console.log("Method Get itinerary faster with place");
        console.log("--------------------");
        var message = "Place: " + ContactDetailsPlace.place + "; Start address: " + ContactDetailsPlace.startAddress + "; End address: " + ContactDetailsPlace.endAddress;
        console.log(message);
        res.status(201).send(jsonMessage(message));
    }
}

