@REM cd notifier_service && npm start && cd ../ & call docker compose --env-file .env up

if not "%1" == "" goto :%1

start "notifier" "%~dpfx0" notifier
start "docker" "%~dpfx0" docker
start "user" "%~dpfx0" user
goto :eof

:notifier
cd notifier_service && npm start
echo notifier
pause
exit

:docker
docker compose --env-file .env up
echo docker
pause
exit

:user
cd ../user_service_rest && docker compose up
echo user
pause
exit